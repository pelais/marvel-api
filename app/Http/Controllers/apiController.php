<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Carbon\Carbon;

class apiController extends Controller{   

    public function index(){
        
        $request = self::url('characters');

        return view('welcome')            
                ->with('copyright', $request->copyright)
                ->with('attributionText', $request->attributionText)
                ->with('attributionHTML', $request->attributionHTML)
                ->with('data', $request->data->results);   
    }

    public function characters($id){

        $request = self::url('characters/'.$id);

        $comics  = self::url('characters/'.$id.'/comics');
        $events  = self::url('characters/'.$id.'/events');
        $series  = self::url('characters/'.$id.'/series');
        $stories = self::url('characters/'.$id.'/stories');


        return view('characters')
                ->with('copyright', $request->copyright)
                ->with('attributionText', $request->attributionText)
                ->with('attributionHTML', $request->attributionHTML)
                ->with('data',    $request->data->results) 
                ->with('comics',  $comics->data->results)                 
                ->with('events',  $events->data->results)                 
                ->with('series',  $series->data->results)                 
                ->with('stories', $stories->data->results);
                
    }

    public function comics($id){

        $request = self::url('comics/'.$id);       

        return view('details')
                ->with('copyright', $request->copyright)
                ->with('attributionText', $request->attributionText)
                ->with('attributionHTML', $request->attributionHTML)
                ->with('data',    $request->data->results)
                ->with('creators', $request->data->results[0]->creators->items)
                ->with('characters', $request->data->results[0]->characters->items);
                
    }

    public function stories($id){

        $request = self::url('stories/'.$id);

        return view('details')
                ->with('copyright', $request->copyright)
                ->with('attributionText', $request->attributionText)
                ->with('attributionHTML', $request->attributionHTML)
                ->with('data',    $request->data->results)
                ->with('creators', $request->data->results[0]->creators->items)
                ->with('characters', $request->data->results[0]->characters->items);
                
    }

    public function series($id){

        $request = self::url('series/'.$id);

        return view('details')
                ->with('copyright', $request->copyright)
                ->with('attributionText', $request->attributionText)
                ->with('attributionHTML', $request->attributionHTML)
                ->with('data',    $request->data->results)
                ->with('creators', $request->data->results[0]->creators->items)
                ->with('characters', $request->data->results[0]->characters->items);                
    
    }
  
    public function url($item){

        $client = new Client();

        try{

            $request = $client->request('GET',env('APP_MARVEL_URI').$item,[
                'query' => [
                    'apikey' => env('APP_MARVEL_KEY'), 
                    'ts'     => Carbon::now()->timestamp, 
                    'hash'   => md5(Carbon::now()->timestamp . env('APP_MARVEL_PRIVATE_KEY') . env('APP_MARVEL_KEY'))
                ]
            ]);
            
            $request = json_decode($request->getBody());           

            if(!$request){
                
                return false;
            }

            return $request;

        }catch(\Exception $e){

            \Log::error(['arquivo' => $e->getFile(), 'erro' => $e->getMessage(), 'linha' => $e->getLine(), 'trace' => $e->getTraceAsString()]);
            return false;
        } 

    }
}