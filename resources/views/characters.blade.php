<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{$attributionText}}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <!-- Styles -->
        <link href="../css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="content">
            <div class="title m-b-md m-t-md">
                <img src="https://logodownload.org/wp-content/uploads/2017/05/marvel-logo.png" width="20%">
            </div>       
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">                    
                        @foreach ($data as $character)
                            <img src="{{$character->thumbnail->path.'.'.$character->thumbnail->extension}}" width="50%">
                            <h1>{{$character->name}}</h1>
                            <p class="desription">{{$character->description}}</p>  
                        @endforeach
                        <a class="btn btn-primary"  href="/" role="button">
                                Página Inicial
                            </a>
                    </div>
                    <div class="col-md-6">                        
                        <div class="attributes">
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapseSeries" role="button" aria-expanded="false" aria-controls="collapseSeries">
                                Series
                            </a>
                            <div class="collapse" id="collapseSeries">
                                <div class="card card-body">
                                @foreach ($series as $serie)
                                <a href="{{route('series', ['id' => $serie->id])}}">
                                    <p>{{$serie->title}}</p>
                                @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="attributes">
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapseStories" role="button" aria-expanded="false" aria-controls="collapseStories">
                                Histórias
                            </a>
                            <div class="collapse" id="collapseStories">
                                <div class="card card-body">
                                @foreach ($stories as $storie)
                                <a href="{{route('stories', ['id' => $storie->id])}}">
                                    <p>{{$storie->title}}</p>
                                </a>
                                @endforeach
                                </div>
                            </div>
                        </div> 
                        <div class="attributes">
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapseComics" role="button" aria-expanded="false" aria-controls="collapseComics">
                                Quadrinhos
                            </a>
                            <div class="collapse" id="collapseComics">
                                <div class="card card-body">
                                 @foreach ($comics as $comic)
                                    <a href="{{route('comics', ['id' => $comic->id])}}">
                                        <p>{{$comic->title}}</p>
                                        </a>
                                @endforeach
                                </div>
                            </div>
                        </div>                      
                    </div>
                </div> 
            </div>
            <div class='footer'>
                {{$attributionText}}
            </div>
        </div>
    </body>
</html>
