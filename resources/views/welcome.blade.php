<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{$attributionText}}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- Styles -->
        <style>           
        </style>
    </head>
    <body>
        <div class="flex-center position-ref">
            <div class="content">
                <div class="title m-b-md m-t-md">
                    <img src="https://logodownload.org/wp-content/uploads/2017/05/marvel-logo.png" width="20%">
                </div>                
                <table class="table table-dark" id="table_custom">
                    <thead>
                        <tr>                        
                            <th scope="col">Nome</th>
                            <th scope="col">Miniatura</th>
                            <th scope="col">Visualizar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $character)
                            <tr>                                
                                <td> {{$character->name}} </td>
                                <td><img class="img_character" src="{{$character->thumbnail->path.'.'.$character->thumbnail->extension}}"> </td>
                                <td><a href="{{route('characters', ['id' => $character->id])}}">Visualizar</a></td>
                            </tr>
                        @endforeach               
                    </tbody>
                </table>
                <div class='footer'>
                  {{$attributionText}}    
            </div>
            </div>
        </div>
    </body>
</html>
