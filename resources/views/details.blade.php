<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{$attributionText}}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <!-- JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <!-- Styles -->
        <link href="../css/style.css" rel="stylesheet">
    </head>
    <body>
        <div class="content">
            <div class="title m-b-md m-t-md">
                <img src="https://logodownload.org/wp-content/uploads/2017/05/marvel-logo.png" width="20%">
            </div>       
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        @foreach ($data as $data)
                            @if($data->thumbnail)
                                <img src="{{$data->thumbnail->path.'.'.$data->thumbnail->extension}}" width="50%">
                            @endif
                            <h1>{{$data->title}}</h1>
                            <p class="desription">{{$data->description}}</p>  
                        @endforeach
                        <a class="btn btn-primary"  onclick="window.history.go(-1); return false;"  href="#" role="button">
                           Voltar
                        </a>
                       
                        <a class="btn btn-primary"  href="/" role="button">
                            Página Inicial
                        </a>
                    </div>
                    <div class="col-md-6">
                        <div class="attributes">
                            <h2>Criadores</h2>
                            @foreach ($creators as $creators)
                                <p class="creatros">{{$creators->name}} <small>({{$creators->role}})</small></p>
                            @endforeach
                            <h2>Personagens</h2>
                            @foreach ($characters as $characters)
                                <p class="characters">{{$characters->name}}</p>
                            @endforeach
                        </div>
                    </div>                        
                </div> 
            </div>
            <div class='footer'>
                {{$attributionText}}
            </div>
        </div>
    </body>
</html>
