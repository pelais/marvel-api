<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'apiController@index']);
Route::get('/characters/{id}', ['as' => 'characters','uses' => 'apiController@characters']);
Route::get('/comics/{id}', ['as' => 'comics','uses' => 'apiController@comics']);
Route::get('/stories/{id}', ['as' => 'stories','uses' => 'apiController@stories']);
Route::get('/series/{id}', ['as' => 'series','uses' => 'apiController@series']);